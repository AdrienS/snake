#include "game.h"

static Game game;
static int highscores[10];
Snake *python;
Snake *cobra;

Game get_game(void) {
    return game;
}

void set_game_state(enum State state) {
    game.state = state;
}

void set_game_score(int score) {
    game.score += pow(2, score);
}

void set_game_level(int level) {
    game.level = level;
}

int* get_highscores() {
    return highscores;
}

/* ================== Input Handling ================== */

void handle_input(void) {
    SDL_Event event;
    
    enum Direction python_direction = (*get_snake(PYTHON))->direction;
    enum Direction cobra_direction = RIGHT;
    if (*get_snake(COBRA)) {
         cobra_direction = (*get_snake(COBRA))->direction;
    }

    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                dealloc_snake(&python);
                SDL_Quit();
                exit(EXIT_SUCCESS);
                break;

            case SDL_KEYDOWN:
                switch (event.key.keysym.sym) {
                    // Pause/resume game
                    case SDLK_p:
                        if (game.state == RUNNING) {
                            set_game_state(PAUSED);
                        } else {
                            set_game_state(RUNNING);
                        }
                        break;
                    // Save game
                    case SDLK_s:
                        set_game_state(SAVE);
                        break;
                    // Load game
                    case SDLK_l:
                        set_game_state(LOAD);
                        break;
                    // Quit game
                    case SDLK_ESCAPE:
                        game.state = QUIT;
                        break;
                    // Move snake head up
                    case SDLK_UP:
                        if (python_direction != DOWN) {
                            change_direction(&python,UP);
                        }
                        break;
                     // Move snake head down
                    case SDLK_DOWN:
                        if (python_direction != UP) {
                            change_direction(&python, DOWN);
                        }
                        break;
                     // Move snake head right
                    case SDLK_LEFT:
                        if (python_direction != RIGHT) {
                            change_direction(&python, LEFT);
                        }
                        break;
                     // Move snake head right
                    case SDLK_RIGHT:
                        if (python_direction != LEFT) {
                            change_direction(&python, RIGHT);
                        }
                        break;
                    // Activate co-op mode
                    case SDLK_SPACE:
                        alloc_snake(get_snake(COBRA));
                        cobra = (*get_snake(COBRA));
                        break;
                    // Control Cobra
                    case SDLK_r:
                        if (cobra && cobra_direction != DOWN) {
                            change_direction(&cobra,UP);
                        }
                        break;
                        
                    case SDLK_d:
                        if (cobra && cobra_direction != RIGHT) {
                            change_direction(&cobra, LEFT);
                        }
                        break;
                        
                    case SDLK_f:
                        if (cobra && cobra_direction != UP) {
                            change_direction(&cobra, DOWN);
                        }
                        break;
                        
                    case SDLK_g:
                        if (cobra && cobra_direction != LEFT) {
                            change_direction(&cobra, RIGHT);
                        }
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }
}

/* ================== Game Loop ================== */

void run_game(int width, int height) {
    
    // Set stage width and height
    set_dimensions(width, height);

    // Set game start time
    clock_t start_time = clock();

    // Init GUI
    initialize_gui(width, height + PANE_HEIGHT); /* Add pane height to show game info */

    // Spawn first food
    spawn_food(NORMAL);

    // Allocate walls
    alloc_wall();
    spawn_wall(HORIZONTAL, (Coordinate){0, 10}, get_width());
    spawn_wall(VERTICAL, (Coordinate){50, 50}, 1);

    // Allocate snake
    alloc_snake(get_snake(PYTHON));
    python = (*get_snake(PYTHON));

    // Set game start information
    set_game_state(RUNNING);
    game.score = 0;
    game.level = 1;

    while (1) {
        // Update play time
        game.played_time = round(difftime(clock(), start_time)) / 6000;
        SDL_Delay(100);
        // Game state machine
        switch (game.state) {
            case RUNNING:
                handle_input();
                update_stage();
                
                // Update snake
                update_snake(&python);
                collision_check(&python, get_food_coordinate(NORMAL), get_food_coordinate(SPECIAL), get_wall_coordinates(), get_wall_length());
                if (cobra) {
                    update_snake(&cobra);
                    collision_check(&cobra, get_food_coordinate(NORMAL), get_food_coordinate(SPECIAL), get_wall_coordinates(), get_wall_length());
                }
                // Update food/special
                decay_special();
                if ((game.played_time % 200) == 0) {
                    set_game_state(BONUS);
                }
                printf("x: %i, y: %i\n", python->segments->x, python->segments->y);
                break;

            case PAUSED:
                update_stage();
                handle_input();
                break;

            case SAVE:
                save_game();
                set_game_state(PAUSED);
                break;

            case LOAD:
                load_game();
                set_game_state(PAUSED);
                break;
                
            case BONUS:
                spawn_food(SPECIAL);
                set_game_state(RUNNING);
                break;
                
            case GAME_OVER:
                load_highscores();
                sort_highscores();
                save_highscores();
                draw_highscores();
                set_game_state(MENU);
                break;
                
            case MENU:
                handle_input();
                break;
                
            case QUIT:
                dealloc_snake(&python);
                if (cobra) {
                    dealloc_snake(&cobra);
                }
                dealloc_stage();
                SDL_Quit();
                exit(EXIT_SUCCESS);
                break;

            default:
                break;
        }
    }
}
