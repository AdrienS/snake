//
//  data.h
//  Snake
//
//  Created by Adrien Schautteet on 11/01/17.
//  Copyright © 2017 Adrien Schautteet. All rights reserved.
//

#ifndef data_h
#define data_h

#include <stdio.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include "game.h"

void load_highscores(void); /* highscores to array */
void sort_highscores(void);
void save_highscores(void);
int* get_highscores(void);
void check_data_dir(void);


#endif /* data_h */
