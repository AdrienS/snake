#include "stage.h"

static Food *food;
static Food *special;

static Walls walls;
static Stage stage;

/* ================== Food ================== */
Coordinate *get_food_coordinate(enum Food_type type) {
    if (type == NORMAL) {
        return &food->coordinate;
    }
    return &special->coordinate;
}

void set_food_coordinate(enum Food_type type, Coordinate coordinate) {
    if (type == NORMAL) {
        food->coordinate.x = coordinate.x;
        food->coordinate.y = coordinate.y;
    } else {
        special = (Food*) malloc(sizeof(Food));
        special->coordinate.x = coordinate.x;
        special->coordinate.y = coordinate.y;
    }
}

void spawn_food(enum Food_type type) {
    // Make sure random x and y are consistent with snake coordinates
    int rand_x = (rand() % get_width()) * SEGMENT_SIZE;
    int rand_y = (rand() % get_height()) * SEGMENT_SIZE;
    
    // Check if random positions are not wall positions
    while (is_wall_position(rand_x, rand_y)) {
        rand_x = (rand() % get_width()) * SEGMENT_SIZE;
        rand_y = (rand() % get_height()) * SEGMENT_SIZE;
    }
    
    // Allocate food
    Food **selected;
    if (type == NORMAL) {
        selected = &food;
    } else {
        selected = &special;
    }
    
    /* Check if food got allocated or
     * check if there is sufficient memory for allocation
     */
    if (!(*selected)) {
        *selected = (Food*) malloc(sizeof(Food));
    } else if (!selected) {
        printf("Error: Insufficient memory.\n");
        exit(EXIT_FAILURE);
    }
    // Set the food coordinates
    (*selected)->coordinate.x = rand_x;
    (*selected)->coordinate.y = rand_y;
    
    if (type == SPECIAL) {
        // Set food birth time
        (*selected)->birth_time = clock();
        // Set food life span
        (*selected)->life_time = 100;
    }
}

void decay_special(void) {
    if (special != NULL) {
        // Get current processor time
        clock_t time = clock();
    
        // Calculate difference between birth of food and current processor time
        int difference = round(difftime(time, special->birth_time)) / 6000;
    
        // Food decays at 100 processor seconds, spawns fresh food
        if (difference > special->life_time) {
            special = NULL;
        }
    }
}

void remove_special(void) {
    special = NULL;
}

bool is_bonus(void) {
    return special;
}

/* ================== Wall ================== */

void alloc_wall(void) {
    // Set initial wall array length
    walls.array_length = 0;
    // Alloc array
    walls.wall_array =
        (Coordinate**) malloc(walls.array_length * sizeof(Coordinate*));
    // Check if there is sufficient memory for allocation
    if (!walls.wall_array) {
        printf("Error: Insufficient memory.\n");
        exit(EXIT_FAILURE);
    }
}

void spawn_wall(enum Wall_type type, Coordinate coordinate, int length) {
    // Get original length
    int original_length = walls.array_length;
    
    // Check if new wall is within bounds
    if ((type == HORIZONTAL && coordinate.x/10 + length > get_width()) ||
        (type == VERTICAL && coordinate.y/10 + length > get_height())) {
        printf("Error: One of your walls is out of the screen bounds.\n");
        exit(EXIT_FAILURE);
    }
    
    // Calculate new length
    int new_length = original_length + length;
    // Update wall struct with new length
    walls.array_length = new_length;
    // Realloc array
    walls.wall_array =
        (Coordinate**) realloc(walls.wall_array, new_length * sizeof(Coordinate*));
    // Check if there is sufficient memory for allocation
    if (!walls.wall_array) {
        printf("Error: Insufficient memory.\n");
        exit(EXIT_FAILURE);
    }
    switch (type) {
        case HORIZONTAL:
            for (int i = original_length; i < new_length; i++) {
                walls.wall_array[i] = (Coordinate*)malloc(sizeof(Coordinate));
                int x = coordinate.x + (i * SEGMENT_SIZE);
                int y = coordinate.y;
                *(walls.wall_array[i]) = (Coordinate){x, y};
            }
            break;
            
        case VERTICAL:
            for (int i = original_length; i <= new_length; i++) {
                walls.wall_array[i] = (Coordinate*)malloc(sizeof(Coordinate));
                int x = coordinate.x;
                int y = coordinate.y + (i * SEGMENT_SIZE);
                *(walls.wall_array[i]) = (Coordinate){x, y};
            }
            break;
            
        default:
            break;
    }
}

Coordinate **get_wall_coordinates(void) {
    return walls.wall_array;
}

int get_wall_length(void) {
    return walls.array_length;
}

bool is_wall_position(int x, int y) {
    for (int i = 0; i < get_wall_length(); i++) {
        if (get_wall_coordinates()[i]->x == x && get_wall_coordinates()[i]->y == y) {
            return true;
        }
    }
    return false;
}

/* ================== Stage ================== */

void set_dimensions(int width, int height) {
    stage.width = width;
    stage.height = height;
}

int get_width(void) {
    return stage.width;
}
int get_height(void) {
    return stage.height;
}

void dealloc_stage(void) {
    free(food);
    free(special);
    free(walls.wall_array);
}
