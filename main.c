//
//  main.c
//  Snake
//
//  Created by Adrien Schautteet on 14/12/16.
//  Copyright © 2016 Adrien Schautteet. All rights reserved.
//

#include <stdio.h>
#include "game.h"

int main(int argc, char * argv[]) {
    srand((int)time(NULL));
    int width = DEFAULT_WIDTH;
    int height = DEFAULT_HEIGHT;
    
    if (argc == 3) {
        width = atoi(argv[1]);
        height = atoi(argv[2]);
    }
    
    run_game(width, height);
    return 0;
}
