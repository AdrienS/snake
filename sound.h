//
//  sound.h
//  Snake
//
//  Created by Adrien Schautteet on 11/01/17.
//  Copyright © 2017 Adrien Schautteet. All rights reserved.
//

#ifndef sound_h
#define sound_h

#include <stdio.h>
#include <stdbool.h>

#include <SDL/SDL_mixer.h>

bool init_sound(void);
void play_beep(void);
void dealloc_sound(void);

#endif /* sound_h */
