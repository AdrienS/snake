//
//  sound.c
//  Snake
//
//  Created by Adrien Schautteet on 11/01/17.
//  Copyright © 2017 Adrien Schautteet. All rights reserved.
//

#include "sound.h"

// beep source: https://www.freesound.org/people/Greencouch/sounds/124905/

static Mix_Chunk *beep;

bool init_sound() {
    if (Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 )
    {
        return false;
    }
    return true;
}

void play_beep() {
    beep = Mix_LoadWAV("Sounds/beep.wav");
    if (!beep) {
        printf("Error loading sound.\n");
        return;
    } else {
        Mix_PlayChannel(-1, beep, 0);
    }
}

void dealloc_sound(void) {
    Mix_FreeChunk(beep);
    Mix_CloseAudio();
}
