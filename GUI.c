#include "GUI.h"

static SDL_Surface *window;
static TTF_Font *font;
static SDL_Color color_white;
const int pane_height = SEGMENT_SIZE*PANE_HEIGHT;

void clear_screen() {
	SDL_FillRect(window, NULL, SDL_MapRGB(window->format, 52, 73, 94));
	SDL_Flip(window);
}

void draw_pane() {
    SDL_Rect pane = {0, window->h-pane_height, window->w, pane_height};
    SDL_FillRect(window, &pane, SDL_MapRGB(window->format, 44, 62, 80));
    
    // Draw level and score labels
    draw_text(color_white, pane.x, pane.y+pane.h/2, "Level: ");
    draw_text(color_white, pane.x+pane.w/2, pane.y+pane.h/2, "Score: ");
    
    char* value;
    // Update level
    value = my_itoa(get_game().level, 10);
    draw_text(color_white, 45, window->h-pane_height/2, value);
    
    // Update score
    value = my_itoa(get_game().score, 10);
    draw_text(color_white, window->w/2+45, window->h-pane_height/2, value);
}

void draw_highscores() {
    SDL_Rect pane = {window->w*.1, window->h*.1, window->w*.8, window->h*.8};
    SDL_FillRect(window, &pane, SDL_MapRGB(window->format, 127, 140, 141));
    
    // Draw highscore label
    draw_text(color_white, pane.x+pane.w/2, pane.y, "HIGHSCORES");
    
    // Construct highscore entry
    for (int i = 0; i < 10; i++) {
        char *entry;
        strcpy(entry, my_itoa(i+1, 10));
        strcat(entry, ". ");
        strcat(entry, my_itoa(get_highscores()[i], 10));
        draw_text(color_white, pane.x + (pane.x*0.1), pane.y + ((pane.x*0.1) + i*get_height()*0.9), entry);
    }
    SDL_Flip(window);
}

void draw_snake(enum Type type) {
    
    Segment *current;
    SDL_Color color;
    
    if (type == PYTHON) {
        // Get the snake segments
        current = (*get_snake(PYTHON))->segments;
        color = (SDL_Color){155, 89, 182}; // Purple
    } else {
        current = (*get_snake(COBRA))->segments;
        color = (SDL_Color){52, 152, 219}; // Blue
    }
    
    SDL_Rect segment = {0, 0, SEGMENT_SIZE, SEGMENT_SIZE};
    while (current) {
        segment.x = current->x;
        segment.y = current->y;
        
        // Give head segment, even segments and odd segments different colors
        if (current->index == 0) {
            SDL_FillRect(window, &segment, SDL_MapRGB(window->format, color.r, color.g, color.b));
        } else if (current->index % 2 == 0) {
            SDL_FillRect(window, &segment, SDL_MapRGB(window->format, 192, 57, 43));
        } else {
            SDL_FillRect(window, &segment, SDL_MapRGB(window->format, 231, 76, 60));
        }
        current = current->next;
    }
}

void draw_food() {
    // Get food coordinates
    Coordinate *food_coordinates = get_food_coordinate(NORMAL);
    SDL_Rect food = {food_coordinates->x, food_coordinates->y, SEGMENT_SIZE, SEGMENT_SIZE};
    SDL_FillRect(window, &food, SDL_MapRGB(window->format, 46, 204, 113));
    
    // Get special coordinate
    if (is_bonus()) {
        Coordinate *special_coordinates = get_food_coordinate(SPECIAL);
        SDL_Rect special = {special_coordinates->x, special_coordinates->y, SEGMENT_SIZE, SEGMENT_SIZE};
        SDL_FillRect(window, &special, SDL_MapRGB(window->format, 241, 196, 15));
    }
}


void draw_walls() {
    // Get wall coordinates
    Coordinate **wall_coordinates = get_wall_coordinates();
    int wall_length = get_wall_length();
    
    for (int i = 0; i < wall_length; i++) {
        SDL_Rect wall = {wall_coordinates[i]->x, wall_coordinates[i]->y, SEGMENT_SIZE, SEGMENT_SIZE};
        SDL_FillRect(window, &wall, SDL_MapRGB(window->format, 236, 240, 241));
    }
}

void update_stage() {
    clear_screen();
    
    // Draw snake(s)
    draw_snake(PYTHON);
    if (*get_snake(COBRA)) {
        draw_snake(COBRA);
    }
    
    // Draw food
    draw_food();
    
    // Draw walls
    draw_walls();
    
    // Draw game info pane
    draw_pane();
    
    SDL_Flip(window);
}

void stop_gui() {
    //TTF_CloseFont(font);
    //SDL_FreeSurface(window); /* Doesnt work, not sure why */
    dealloc_sound();
    TTF_Quit();
	SDL_Quit();
}

/*
 * Initialiseer het venster.
 */
void initialize_window(char *title, int grid_width, int grid_height) {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Could not initialize SDL: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    
    int window_width = grid_width * SEGMENT_SIZE;
    int window_height = grid_height * SEGMENT_SIZE;
    window = SDL_SetVideoMode(window_width, window_height, 0, SDL_HWPALETTE | SDL_DOUBLEBUF);
    if (window == NULL) {
        printf("Couldn't set screen mode to required dimensions: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    /* Set the screen title */
    SDL_WM_SetCaption(title, NULL);
    
    // Init ttf, font, color and sound
    TTF_Init();
    font = TTF_OpenFont(FONT_ARIAL, 12);
    color_white = (SDL_Color) {255, 255, 255};
    init_sound();
}


void initialize_gui(int grid_width, int grid_height) {
	initialize_window("Snake", grid_width, grid_height);
	atexit(stop_gui);
}

void draw_text(SDL_Color color, int x, int y, char* text) {
    SDL_Rect rect = {x, y, 0, 0};
    SDL_Surface *lbl = TTF_RenderText_Solid(font, text, color);
    SDL_BlitSurface(lbl, NULL, window, &rect);
}

// Source: http://www.strudel.org.uk/itoa/
char* my_itoa(int val, int base){
    
    static char buf[32] = {0};
    
    int i = 30;
    
    for(; val && i ; --i, val /= base)
        
        buf[i] = "0123456789abcdef"[val % base];
    
    return &buf[i+1];
}

