//
//  snake.h
//  Snake
//
//  Created by Adrien Schautteet on 14/12/16.
//  Copyright © 2016 Adrien Schautteet. All rights reserved.
//

#ifndef snake_h
#define snake_h

#include <stdio.h>
#include <stdlib.h>
#include "game.h"
#include "stage.h"
#include "sound.h"

enum Type {
    PYTHON,
    COBRA
};

enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
};

typedef struct Segment {
    int index;
    int x;
    int y;
    struct Segment *next;
    struct Segment *prev;
} Segment;

typedef struct Snake {
    int length;
    float speed;
    Segment *segments;
    enum Direction direction;
    Segment *head;
    Segment *tail;
} Snake;


void alloc_snake(Snake**);
void dealloc_snake(Snake**);
void grow_snake(Snake**, int);
void shrink_snake(Snake**, int);
void update_snake(Snake**);
void collision_check(Snake**, const Coordinate*, const Coordinate*, Coordinate**, int);
void change_direction(Snake**, enum Direction);
Snake **get_snake(enum Type);

#endif /* snake_h */
