#ifndef GUI_H_
#define GUI_H_

#include <math.h>
#include "SDL/SDL.h"
#include "SDL/SDL_ttf.h"
#include "game.h"
#include "stage.h"
#include "snake.h"
#include "sound.h"

#define FONT_ARIAL "Fonts/arial.ttf"

void initialize_gui(int grid_width, int grid_height);

void draw_pane(void);           /* Draws pane with game info */
void draw_highscores(void);     /* Draws pane with highscores */

void draw_snake(enum Type);
void draw_food(void);
void draw_walls(void);

void update_stage(void);

// Function that packs SDL procedures together to draw text to screen
void draw_text(SDL_Color, int, int, char*);

// Source: http://www.strudel.org.uk/itoa/
char* my_itoa(int val, int base);

#endif /* GUI_H_ */
