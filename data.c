//
//  data.c
//  Snake
//
//  Created by Adrien Schautteet on 11/01/17.
//  Copyright © 2017 Adrien Schautteet. All rights reserved.
//

#include "data.h"

#define FILE_PATH "Data/save_data.txt"
#define HSCORE_PATH "Data/score_data.txt"

/* ================== Persistent Data Handling ================== */

void save_game(void) {
    
    // Check if data folder exist
    check_data_dir();
    
    // Set default write mode & file location
    char *mode = "w";
    char *file_name = FILE_PATH;
    // Open the file
    FILE *save_file = fopen(file_name, mode); /** TODO: check fopen return */
    
    // Write to file
    fprintf(save_file, "Width: %i\n", get_width());
    fprintf(save_file, "Heigth: %i\n", get_height());
    fprintf(save_file, "Current score: %i\n", get_game().score);
    fprintf(save_file, "Current level: %i\n", get_game().level);
    fprintf(save_file, "Snake length: %i\n", (*get_snake(PYTHON))->length);
    fprintf(save_file, "Food coordinates: [%i, %i]\n", get_food_coordinate(NORMAL)->x, get_food_coordinate(NORMAL)->y);
    if (get_food_coordinate(SPECIAL)) {
        fprintf(save_file, "Special food coordinates: [%i, %i]\n", get_food_coordinate(SPECIAL)->x, get_food_coordinate(SPECIAL)->y);
    } else {
        fprintf(save_file, "Special food coordinates: [-1, -1]\n");
    }
    Segment *current = (*get_snake(PYTHON))->segments;//->next;
    for (int i = 0; current; i++) {
        fprintf(save_file, "Segment coordinates: [%i, %i]\n",current->x, current->y);
        current = current->next;
    }
    fclose(save_file);
}

void load_game(void) {
    
    // Set default write mode & file location
    char *mode = "r";
    char *file_name = FILE_PATH;
    
    // Open the file
    FILE *save_file = fopen(file_name, mode); /** TODO: check fopen return */
    if (!save_file) {
        printf("Error opening file. Exiting!");
        exit(EXIT_FAILURE);
    }
    
    // Variables - data to extract from file
    int width;
    int height;
    int score;
    int level;
    int length;
    Coordinate foodc;
    Coordinate specialc;
    
    // Extract data and put it in defined variables
    fscanf(save_file,
           "Width: %i\
           Heigth: %i\
           Current score: %i\
           Current level: %i\
           Snake length: %i\
           Food coordinates: [%i, %i]\
           Special food coordinates: [%i, %i]",
           &width, &height, &score, &level, &length, &foodc.x, &foodc.y, &specialc.x, &specialc.y);
    
    // Setup game
    set_dimensions(width, height);
    initialize_gui(width, height + PANE_HEIGHT);
    set_game_score(score);
    set_game_level(level);
    set_food_coordinate(NORMAL, (Coordinate){foodc.x, foodc.y});
    if (specialc.x != -1 && specialc.y != -1) {
        set_food_coordinate(SPECIAL, (Coordinate){specialc.x, specialc.y});
    }
    
    // Grow/shrink snake if it needs growing/shrinking
    int current_length = (*get_snake(PYTHON))->length;
    int difference = abs(current_length - length);
    if (current_length < length) {
        grow_snake(get_snake(PYTHON), difference);
    } else {
        shrink_snake(get_snake(PYTHON), difference);
    }
    
    
    // Rebuild saved snake
    char c;
    char coordinate_buffer[3];
    int i = 0;
    Segment *current = (*get_snake(PYTHON))->segments;
    while ((c = fgetc(save_file)) != EOF) {
        if (isdigit(c)) {
            coordinate_buffer[i] = c;
            i++;
        } else if (c == ',') {
            //printf("%i\n", atoi(coordinate_buffer));
            current->x = atoi(coordinate_buffer);
            memset(coordinate_buffer, 0, 3);
            i = 0;
        } else if (c == ']') {
            current->y = atoi(coordinate_buffer);
            current = current->next;
            memset(coordinate_buffer, 0, 3);
            i = 0;
        }
    }
}

void nullify_highscores(void) {
    // Check if data folder exist
    check_data_dir();
    
    // Set default write mode & file location
    char *mode = "w";
    char *file_name = HSCORE_PATH;
    // Open the file
    FILE *score_file = fopen(file_name, mode);
    if (!score_file) {
        printf("Error opening file. Exiting!");
        exit(EXIT_FAILURE);
    }
    
    for (int i = 0; i < 10; i++) {
        fprintf(score_file, "0\n");
    }
    fclose(score_file);
}

void load_highscores(void) {
    
    // If file doesnt exist, make new one with zeros
    if (access(HSCORE_PATH, F_OK) == -1) {
        nullify_highscores();
    }
    
    // Set default write mode & file location
    char *mode = "r";
    char *file_name = HSCORE_PATH;
    // Open the file
    FILE *score_file = fopen(file_name, mode);
    if (!score_file) {
        printf("Error opening file. Exiting!");
        exit(EXIT_FAILURE);
    }
    
    char c;
    char score_buffer[3];
    int i = 0;
    int j = 0;
    while ((c = fgetc(score_file)) != EOF) {
        if (isdigit(c)) {
            score_buffer[i] = c;
            i++;
        } else if (c == '\n') {
            get_highscores()[j] = atoi(score_buffer);
            memset(score_buffer, 0, 3);
            i = 0;
            j++;
        }
    }
}

void sort_highscores(void) {
    
    // Get current score
    int score = get_game().score;
    
    // Value to keep to insert in next iteration
    int keep = 0;
    
    // Bool to check if value got inserted in array
    bool inserted = false;
    
    /* Loop consists of two parts:
     *  1. Actions to perform after insert
     *  2. Actions to perform before insert
     */
    for (int i = 0; i < 10; i++) {
        if (inserted && i < 9) {
            int temp = get_highscores()[i];
            get_highscores()[i] = keep;
            keep = temp;
        }
        else if (score > get_highscores()[i]) {
            keep = get_highscores()[i];
            get_highscores()[i] = score;
            inserted = true;
            score = 0; /* Reset score value */
        }
    }
}

void save_highscores() {
    
    // Check if data folder exist
    check_data_dir();
    
    // Set default write mode & file location
    char *mode = "w";
    char *file_name = HSCORE_PATH;
    // Open the file
    FILE *score_file = fopen(file_name, mode);
    if (!score_file) {
        printf("Error opening file. Exiting!");
        exit(EXIT_FAILURE);
    }
    
    // Print highscore values
    for (int i = 0; i < 10; i++) {
        fprintf(score_file, "%i\n", get_highscores()[i]);
    }
    fclose(score_file);
}

void check_data_dir(void) {
    // Makes data folder if it doesnt exist
    DIR* data_folder = opendir("Data");
    if (data_folder) {
        closedir(data_folder);
    } else {
        mkdir("Data/", 0777);
    }
}
