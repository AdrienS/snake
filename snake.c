//
//  snake.c
//  Snake
//
//  Created by Adrien Schautteet on 14/12/16.
//  Copyright © 2016 Adrien Schautteet. All rights reserved.
//

#include "snake.h"

// Source: https://www.lemoda.net/c/modulo-operator/
#define MOD(a,b) ((((a)%(b))+(b))%(b))

static Snake *python;
static Snake *cobra;

void alloc_snake(Snake **snake) {
    
    // Create first segment
    Segment *first = (Segment*) malloc(sizeof(Segment));
    
    // Check if there is sufficient memory for allocation
    if (!first) {
        printf("Error: Insufficient memory.");
        exit(EXIT_FAILURE);
    }
    
    // Set first segment values
    first->x = 0;
    first->y = 0;
    first->next = NULL;
    first->prev = NULL;
    first->index = 0;
    
    // Create snake
    *snake = (Snake *) malloc(sizeof(Snake));
    
    // Check if there is sufficient memory for allocation
    if (!snake) {
        printf("Error: Insufficient memory.");
        exit(EXIT_FAILURE);
    }
    
    // Set snake info
    (*snake)->head = first;
    (*snake)->tail = first;
    (*snake)->length = 1;
    (*snake)->speed = .1f;
    (*snake)->segments = first;
    (*snake)->direction = RIGHT;
}

void dealloc_snake(Snake **snake) {
    // Get snake head segment
    Segment *current = (*snake)->head;
    Segment *next;
    
    // Loop segments and free them
    while (current) {
        // Set next to next of current
        next = current->next;
        // Free current
        free(current);
        // Set current to next
        current = next;
    }
    free(*snake);
}

void grow_snake(Snake **snake, int length) {
    while (length > 0) {
        // Create new segment
        Segment *new = (Segment*) malloc(sizeof(Segment));
        
        // Check if there is sufficient memory for allocation
        if (!new) {
            printf("Error: Insufficient memory.");
            exit(EXIT_FAILURE);
        }
        
        // Set new segment values
        new->x = (*snake)->tail->x;
        new->y = (*snake)->tail->y;
        new->next = NULL;
        new->prev = (*snake)->tail;
        new->index = (*snake)->length;
        
        // Update tail info
        (*snake)->tail->next = new;
        (*snake)->tail = new;
        
        // Increment snake length
        (*snake)->length++;
        length--;
    }
}

void shrink_snake(Snake **snake, int length) {
    while (length > 0 && (*snake)->length > 1) {
        // Remove segment
        Segment *previous = (*snake)->tail->prev;
        free((*snake)->tail);
        previous->next = NULL;
        
        // Update tail info
        (*snake)->tail = previous;

        length--;
        (*snake)->length--;
    }
}

void update_snake(Snake **snake) {
    
    // Movement variables
    int horizontal_movement;
    int vertical_movement;

    // Get the snake head segment and positions
    Segment *current = (*snake)->head;
    int x_pos = current->x;
    int y_pos = current->y;
    
    // Set direction
    switch ((*snake)->direction) {
        case UP:
            vertical_movement = -10;
            horizontal_movement = 0;
            break;
            
        case DOWN:
            vertical_movement = 10;
            horizontal_movement = 0;
            break;
            
        case LEFT:
            horizontal_movement = -10;
            vertical_movement = 0;
            break;
            
        case RIGHT:
            horizontal_movement = 10;
            vertical_movement = 0;
            break;
            
        default:
            break;
    }
    
    // Set previous position
    int prev_x = current->x;
    int prev_y = current->y;
    // Set current position
    current->x = MOD(x_pos + horizontal_movement, get_width() * SEGMENT_SIZE);
    current->y = MOD(y_pos + vertical_movement, get_height() * SEGMENT_SIZE);
    
    // Loop segments and move them to the position of their predecessor
    while (current->next) {
        current = current->next;
        int current_x = current->x;
        int current_y = current->y;
        current->x = prev_x;
        current->y = prev_y;
        prev_x = current_x;
        prev_y = current_y;
    }
}

void collision_check(Snake **snake, const Coordinate *food, const Coordinate *special, Coordinate **walls, int wlength) {
    
    int x = (*snake)->head->x;
    int y = (*snake)->head->y;
    
    // Check collision with food
    if (x == food->x && y == food->y) {
        grow_snake(snake, 1);
        set_game_score(NORMAL_SCORE);
        spawn_food(NORMAL);
        play_beep();
    }
    
    // Check collision with special food
    if (special && x == special->x && y == special->y) {
        grow_snake(snake, 5);
        set_game_score(SPECIAL_SCORE);
        remove_special();
        play_beep();
    }
    
    // Check collision with self
    Segment *current = (*snake)->head->next;
    
    while (current) {
        if (x == current->x && y == current->y && (*snake)->length > 4) {
            printf("Damn son, stop eating yo'self!\n");
            set_game_state(GAME_OVER);
        }
        current = current->next;
    }
    
    // Write code to check collision with obstacles
    for (int i = 0; i < wlength; i++) {
        if (walls[i]->x == x && walls[i]->y == y) {
            printf("Damn son, stop running against walls!\n");
            set_game_state(GAME_OVER);
        }
    }
}

void change_direction(Snake **snake, enum Direction direction) {
    (*snake)->direction = direction;
}

Snake **get_snake(enum Type type) {
    if (type == PYTHON) {
        return &python;
    }
    return &cobra;
}
