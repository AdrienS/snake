#ifndef STAGE_H_
#define STAGE_H_

#define SEGMENT_SIZE 10
#define DEFAULT_WIDTH 20
#define DEFAULT_HEIGHT 20
#define PANE_HEIGHT DEFAULT_HEIGHT/5

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <math.h>

enum Food_type {
    NORMAL,
    SPECIAL
};

enum Wall_type {
    HORIZONTAL,
    VERTICAL
};

typedef struct {
    int x;
    int y;
} Coordinate;

typedef struct {
    int width;
    int height;
} Stage;

typedef struct {
    Coordinate coordinate;
    clock_t birth_time;
    int life_time;
    enum Food_type type;
} Food;

typedef struct {
    Coordinate **wall_array;
    int array_length;
} Walls;

// Food
Coordinate *get_food_coordinate(enum Food_type);
void set_food_coordinate(enum Food_type, Coordinate);
void spawn_food(enum Food_type);
void decay_special(void);
void remove_special(void);
bool is_bonus(void);

// Wall
void alloc_wall(void);
void spawn_wall(enum Wall_type, const Coordinate, int);
Coordinate **get_wall_coordinates(void);
int get_wall_length(void);
bool is_wall_position(int, int);

// Stage
void set_dimensions(int, int);
int get_width(void);
int get_height(void);
void dealloc_stage(void);

#endif /* STAGE_H_ */
