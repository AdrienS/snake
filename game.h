#ifndef GAME_H_
#define GAME_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <string.h>
#include "GUI.h"
#include "snake.h"
#include "stage.h"
#include "data.h"

#define NORMAL_SCORE 0
#define SPECIAL_SCORE 5

enum State {
    RUNNING,
    PAUSED,
    GAME_OVER,
    SAVE,
    LOAD,
    BONUS,
    MENU,
    QUIT
};

typedef struct {
    enum State state;
    int score;
    int level;
    int played_time;
} Game;

Game get_game(void);

void set_game_state(enum State);
void set_game_score(int);
void set_game_level(int);
void handle_input(void);
void run_game(int, int);
void save_game(void);
void load_game(void);
int* get_highscores(void);

#endif /* GAME_H_ */
